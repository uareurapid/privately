//
//  RGBInfo.m
//  TheUnshredder
//
//  Created by PC Dreams on 30/09/2018.
//  Copyright © 2018 PC Dreams. All rights reserved.
//

#import "RGBInfo.h"

@implementation RGBInfo

@synthesize red,green,blue,alpha;

-(id) initWithRed: (int ) redValue green: (int ) greenValue  blue: (int) blueValue  andAlpha: (int) alphaValue{
    
    self = [super init];
    red = redValue;
    green = greenValue;
    blue = blueValue;
    alpha = alphaValue;
    return self;
}

@end
