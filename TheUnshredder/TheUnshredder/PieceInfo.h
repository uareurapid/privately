//
//  PieceInfo.h
//  TheUnshredder
//
//  Created by PC Dreams on 30/09/2018.
//  Copyright © 2018 PC Dreams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface PieceInfo : NSObject

@property (nonatomic, assign) NSInteger leftNeighbourIndex;
@property (nonatomic, assign) NSInteger rightNeighbourIndex; //-1 if none
@property (nonatomic, assign) NSInteger index;

@property (nonatomic, assign) NSInteger numMatchesWithLeftNeighbour;
@property (nonatomic, assign) NSInteger numMatchesWithRightNeighbour;

@property (nonatomic, assign) BOOL alreadyArranged;

-(id) initWithNeighbours: (NSInteger) leftOne right: (NSInteger) rightOne andSelf: (NSInteger) himSelf;

@end

NS_ASSUME_NONNULL_END
