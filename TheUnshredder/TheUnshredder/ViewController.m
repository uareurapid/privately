//
//  ViewController.m
//  TheUnshredder
//
//  Created by PC Dreams on 28/09/2018.
//  Copyright © 2018 PC Dreams. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize imageInfo;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
}


- (IBAction)loadImageClicked:(id)sender {
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary | UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:false completion:nil];
    
    [self.imageView setContentMode:UIViewContentModeScaleAspectFit];
    self.image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        // Update the UI in the main thread
         self.imageView.image = self.image;
    });
   
    
    imageInfo = [[ImageInfo alloc] initWithImage:self.image];
    //divide in smaller pieces
    [imageInfo divideMainImageInPieces];
    //read 1 column of pixels from the left and right of each piece
    [imageInfo readPixelDataFromImageChunks];
    //compare each piece left and right with all the others left and right
    [imageInfo comparePixelDataBetweenPieces];
    //the bigger the number of matches (pixels with same value the better)
    //try to reconstruct the array of chunks into a different image, by swapping the pieces positions
    UIImage *reconstructed = [imageInfo reorganizePieces];
    dispatch_async(dispatch_get_main_queue(), ^{
        // Update the UI in the main thread
        NSLog(@"REBUILDING IMAGE....");
        self.imageView.image = reconstructed;
    });
    
    
}


@end
