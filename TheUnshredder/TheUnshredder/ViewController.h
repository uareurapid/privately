//
//  ViewController.h
//  TheUnshredder
//
//  Created by PC Dreams on 28/09/2018.
//  Copyright © 2018 PC Dreams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageInfo.h"

@interface ViewController : UIViewController <UIImagePickerControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) UIImage* image;
@property (nonatomic, strong) ImageInfo* imageInfo;
- (IBAction)loadImageClicked:(id)sender;

@end

