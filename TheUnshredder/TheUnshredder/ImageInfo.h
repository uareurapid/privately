//
//  ImageInfo.h
//  TheUnshredder
//
//  Created by PC Dreams on 28/09/2018.
//  Copyright © 2018 PC Dreams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PixelData.h"
#import "RGBInfo.h"
#import "PieceInfo.h"

NS_ASSUME_NONNULL_BEGIN

@interface ImageInfo : NSObject

@property (nonatomic, assign) IBOutlet UIImage* image;
@property (nonatomic, assign) NSInteger width;
@property (nonatomic, assign) NSInteger height;
@property (nonatomic, assign) NSInteger numberOfChunks;

@property (nonatomic, strong) NSMutableArray* imagePieces;
@property (nonatomic, strong) NSMutableArray* chunksPixelData;

@property (nonatomic, strong) NSMutableArray *piecesInfoArray;


@property (nonatomic, strong) NSMutableArray* reconstructedImagePieces;

-(id)initWithImage: (UIImage *) img;
-(NSInteger) calculateNumberOfChunks;
-(void) divideMainImageInPieces;
- (UIImage *)cropImageWithRect:(CGRect)rect;
-(void) readPixelDataFromImageChunks;
-(void) comparePixelDataBetweenPieces;
-(UIImage *) reorganizePieces;
-(NSMutableArray *) reorganizePieceInPosition: (PieceInfo *) piece;
-(BOOL) containsIndexNumber: (NSMutableArray *) array number: (NSNumber *) num;

-(RGBInfo *) readRGBValues: (UIColor*) color;
-(NSUInteger) doRGBValuesMatch: (RGBInfo *) firstColor  otherColor: (RGBInfo *) secondColor;
- (UIImage *)mergeImagesFromArrayOfImages: (NSArray *)imagesArray;

-(NSInteger) comparePixelsReference: (NSArray *) pixelDataReference neighbour: (NSArray *) pixelDataNeighbour;
-(PieceInfo *) getPieceAtIndex: (int) index;

@end

NS_ASSUME_NONNULL_END
