//
//  RGBInfo.h
//  TheUnshredder
//
//  Created by PC Dreams on 30/09/2018.
//  Copyright © 2018 PC Dreams. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RGBInfo : NSObject

@property (nonatomic, assign) int red;
@property (nonatomic, assign) int green;
@property (nonatomic, assign) int blue;
@property (nonatomic, assign) int alpha;

-(id) initWithRed: (int ) redValue green: (int ) greenValue  blue: (int) blueValue  andAlpha: (int) alpha;
@end

NS_ASSUME_NONNULL_END
