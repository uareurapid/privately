//
//  PieceInfo.m
//  TheUnshredder
//
//  Created by PC Dreams on 30/09/2018.
//  Copyright © 2018 PC Dreams. All rights reserved.
//

#import "PieceInfo.h"

@implementation PieceInfo


@synthesize leftNeighbourIndex,rightNeighbourIndex,index, numMatchesWithLeftNeighbour,numMatchesWithRightNeighbour,alreadyArranged;

-(id) initWithNeighbours: (NSInteger) leftOne right: (NSInteger) rightOne andSelf: (NSInteger) himSelf  {
    self = [super init];
    leftNeighbourIndex =  leftOne;
    rightNeighbourIndex = rightOne;
    index = himSelf;
    numMatchesWithRightNeighbour = numMatchesWithLeftNeighbour = 0;
    alreadyArranged = false;
    return self;
}
@end
