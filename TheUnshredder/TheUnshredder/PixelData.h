//
//  PixelData.h
//  TheUnshredder
//
//  Created by PC Dreams on 28/09/2018.
//  Copyright © 2018 PC Dreams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//assume the width / 10 as per the example images
#define CHUNK_WIDTH_IN_PIXELS(width) width/10

NS_ASSUME_NONNULL_BEGIN

@interface PixelData : NSObject

@property (nonatomic, strong) UIImage* image;
@property (nonatomic, strong) NSArray* pixelDataLeft;
@property (nonatomic, strong) NSArray* pixelDataRight;

-(id) initWithImage: (UIImage *) img;
-(void) readPixelData;
- (NSArray*)getRGBAsFromImage:(UIImage*)image atX:(int)x andY:(int)y count:(int)count;
@end

NS_ASSUME_NONNULL_END
