//
//  ImageInfo.m
//  TheUnshredder
//
//  Created by PC Dreams on 28/09/2018.
//  Copyright © 2018 PC Dreams. All rights reserved.
//

#import "ImageInfo.h"

@implementation ImageInfo

@synthesize image, width,height, numberOfChunks, imagePieces, chunksPixelData, reconstructedImagePieces, piecesInfoArray;

    -(id)initWithImage: (UIImage *) img {
    
        self = [super init];
        image = img;
        width = image.size.width;
        height = image.size.height;
        return self;
    }

    //number of pieces (by width)
    -(NSInteger) calculateNumberOfChunks {
        numberOfChunks = width / (CHUNK_WIDTH_IN_PIXELS(width));
        return numberOfChunks;
    }

    //divide im small chunks, sized 70 px
    -(void) divideMainImageInPieces {
    
        imagePieces = [[NSMutableArray alloc] initWithCapacity:self.calculateNumberOfChunks];
        //hold the pixel data for each piece
        chunksPixelData = [[NSMutableArray alloc] initWithCapacity: numberOfChunks];
        //holde the pieces in final order
        reconstructedImagePieces = [[NSMutableArray alloc] initWithCapacity: numberOfChunks];
        
        //info about direct neighbours
        piecesInfoArray = [[NSMutableArray alloc]initWithCapacity:numberOfChunks];
      
        NSLog(@"Number of small pieces: %ld",numberOfChunks);
        int xPos = 0;
        for (int i = 0; i < numberOfChunks; i++) {
            CGRect rect = CGRectMake(xPos, 0, CHUNK_WIDTH_IN_PIXELS(width), height);
            imagePieces[i] = [self cropImageWithRect: rect];
            xPos+=CHUNK_WIDTH_IN_PIXELS(width);
        }
        
        NSLog(@"Number of pieces on array: %ld",imagePieces.count);
    }

//get a smaller piece
- (UIImage *)cropImageWithRect:(CGRect)rect
{
    if (image.scale > 1.0f) {
        rect = CGRectMake(rect.origin.x * image.scale,
                          rect.origin.y * image.scale,
                          rect.size.width * image.scale,
                          rect.size.height * image.scale);
    }
    
    CGImageRef imageRef = CGImageCreateWithImageInRect(image.CGImage, rect);
    UIImage *piece = [UIImage imageWithCGImage:imageRef scale:image.scale orientation:image.imageOrientation];
    CGImageRelease(imageRef);
    NSLog(@"PIECE WIDTH: %d and HEIGHT: %d", (int)piece.size.width, (int)piece.size.height);
    return piece;
}

//go 1 by one and read the chunks pixel data
-(void) readPixelDataFromImageChunks{
    for (int i = 0; i < numberOfChunks; i++) {
        PixelData *data = [[PixelData alloc] initWithImage:imagePieces[i]];
        //this data reading could be optimized of course, but that is not the main goal here!
        chunksPixelData[i] = data;
        [data readPixelData ];
        
    }
}

-(void) comparePixelDataBetweenPieces{

    
    for (int i = 0; i < numberOfChunks; i++) {
        //PixelData *current = chunksPixelData[i];
        
        PieceInfo *piece = nil;
        if(i==0 || i == (numberOfChunks - 1) ) {
            if(i==0) {
               piece = [[PieceInfo alloc] initWithNeighbours:-1 right:i+1 andSelf:i];
            }
            else {
               piece = [[PieceInfo alloc] initWithNeighbours:i-1 right:-1 andSelf:i];
            }
            
        }
        else {
            
            piece = [[PieceInfo alloc] initWithNeighbours:i-1 right:i+1 andSelf:i];
        }
        
        piecesInfoArray[i] = piece;
    }
    
    //check how scrumbled it is, compare similar pixel data with direct neighbours
    NSInteger size = piecesInfoArray.count;
    for(int i = 0; i < size; i++) {
        
        //contains info about neighbour positions and number of matches
        PieceInfo *piece = piecesInfoArray[i];
        //pixel data information (left & right) of the current image piece being processed
        PixelData *referencePivot = chunksPixelData[i];
        
        if(piece.leftNeighbourIndex > -1) {
            //it has a left neighbour (compare this one left data, with the left neighbour right data)
            
            PixelData *leftNeighbour = chunksPixelData[piece.leftNeighbourIndex];
            NSArray *pixelDataReferenceLeft = referencePivot.pixelDataLeft;
            NSArray *pixelDataNeighbourRight = leftNeighbour.pixelDataRight;
            piece.numMatchesWithLeftNeighbour = [self comparePixelsReference:pixelDataReferenceLeft neighbour:pixelDataNeighbourRight];
            
        }
        else {
            piece.numMatchesWithLeftNeighbour = 0;
        }
        //it has a right one too (compare this one right data, with the right neighbour left data)
        if(piece.rightNeighbourIndex > -1) {
            
            PixelData *rightNeighbour = chunksPixelData[piece.rightNeighbourIndex];
            NSArray *pixelDataReferenceRight = referencePivot.pixelDataRight;
            NSArray *pixelDataNeighbourLeft = rightNeighbour.pixelDataLeft;
            
            piece.numMatchesWithRightNeighbour = [self comparePixelsReference:pixelDataReferenceRight neighbour:pixelDataNeighbourLeft];
        }
        else {
            piece.numMatchesWithRightNeighbour = 0;
        }
    }
    
    
    
    //first add them all on their initial positions (i could reuse arrays but i´m going for readability instead)
    for(int i = 0; i < numberOfChunks; i++) {
        reconstructedImagePieces[i] = imagePieces[i];
    }
    
    int counter = 0;
    
    
    //now compare all chunks against each other
    for (int i = 0; i < numberOfChunks; i++) {
        
        //pivot
        PixelData *referencePivot = chunksPixelData[i];
        
        //get the original info about this pivot
        PieceInfo *piece = piecesInfoArray[i];
        
        //left data
        NSArray *pixelDataReferenceLeft = referencePivot.pixelDataLeft;
        //right data
        NSArray *pixelDataReferenceRight = referencePivot.pixelDataRight;
        
        NSLog(@"Checking chunk at position %d", i);
         //compare with all the other pieces, except himself
        for(int j = 0; j < numberOfChunks; j++ ) {
            
            if(j != i) { //this is the same, do not compare with himself
                
                counter++;
                
                PixelData *dataSecond = chunksPixelData[j];
                NSArray *pixelDataLeftSecond = dataSecond.pixelDataLeft;
                NSArray *pixelDataRightSecond = dataSecond.pixelDataRight;
                
                //NOTE always comparing as if is -> the pivot against the oponent and not the opposite
                
                NSInteger numMatchesRight = [self comparePixelsReference:pixelDataReferenceRight neighbour:pixelDataLeftSecond];
                NSInteger numMatchesLeft = [self comparePixelsReference:pixelDataReferenceLeft neighbour:pixelDataRightSecond];
                
                 //original number of matches is smaller?
                    if( numMatchesRight > piece.numMatchesWithRightNeighbour) {
                        
                          NSLog(@"NUM ORIGINAL MATCHES OF PIECE %ld WITH RIGHT NEIGHBOUR %ld -> %ld CALCULATED RIGHT NEIGHBOUR %d, MATCHES -> %ld",
                                piece.index,
                                piece.rightNeighbourIndex,
                                piece.numMatchesWithRightNeighbour,
                                j,
                                numMatchesRight);
                        
                            //then maybe this is the correct right neighbour?
                            NSInteger calculatedRightNeighbour = j;
                 
                            //update the number of matches to this value
                            piece.numMatchesWithRightNeighbour = numMatchesRight;
                            //update the index of the neighbour
                            piece.rightNeighbourIndex = calculatedRightNeighbour;
                        
                    }
                    //same for the other side
                    if(numMatchesLeft > piece.numMatchesWithLeftNeighbour) {
                        //maybe this is the correct left neighbour?
                        
                        NSLog(@"NUM ORIGINAL MATCHES OF PIECE %ld WITH LEFT NEIGHBOUR %ld -> %ld CALCULATED LEFT NEIGHBOUR %d, MATCHES -> %ld",
                              piece.index,
                              piece.leftNeighbourIndex,
                              piece.numMatchesWithLeftNeighbour,
                              j,
                              numMatchesLeft);
                        
                        NSInteger calculatedLeftNeighbour = j;
                        piece.numMatchesWithLeftNeighbour = numMatchesLeft;
                        piece.leftNeighbourIndex = calculatedLeftNeighbour;
                        //Example. if new right neighbour is Index 5 -> swap this image with the previous position (5-1=4)
                        
                    }
                    
            }
        }
        
    }
    
    NSLog(@"I HAVE COUNTED %d different pieces while number of chunks = %ld and it should have been %ld", counter, numberOfChunks, (numberOfChunks* (numberOfChunks-1)));

}

-(PieceInfo *) getPieceAtIndex: (int) index {
    return  imagePieces[index];
}

//compare pixels from one image piece to its neighbour image
-(NSInteger) comparePixelsReference: (NSArray *) pixelDataReference neighbour: (NSArray *) pixelDataNeighbour  {
    
    NSInteger matchCount = 0;
    //height is also the number of pixels we have read before
    int counter = 0;
    for(int x = 0 ; x < height; x ++) {
        
        //reference piece x th pixel color on the lef and right
        UIColor *colorReferencePiece  = pixelDataReference[x];
        //second piece
        UIColor *colorNeighbourPiece  = pixelDataNeighbour[x];
        
        RGBInfo *colorReferencePieceRGB = [self readRGBValues:colorReferencePiece];
        RGBInfo *colorNeighbourPieceRGB = [self readRGBValues:colorNeighbourPiece];

        matchCount += [self doRGBValuesMatch:colorReferencePieceRGB otherColor:colorNeighbourPieceRGB];
        counter++;
    }
    
    return matchCount;
}


-(BOOL) containsIndexNumber: (NSMutableArray *) array number: (NSNumber *) num {
    NSInteger size = array.count;
    for(int i = 0; i < size; i++) {
        if([array[i] intValue] == [num intValue]) {
            NSLog(@"Already saw index: %d ", [array[i] intValue]);
            return true;
        }
    }
    return false;
}
-(UIImage *) reorganizePieces{

    /**
     FOR PIECE AT INDEX 0, LEFT BEST MATCH SHOULD BE PIECE 6 WITH 15 MATCHES, AND RIGHT BEST MATCH SHOULD BE PIECE 5 WITH 24 MATCHES
     FOR PIECE AT INDEX 1, LEFT BEST MATCH SHOULD BE PIECE 8 WITH 14 MATCHES, AND RIGHT BEST MATCH SHOULD BE PIECE 3 WITH 29 MATCHES
     FOR PIECE AT INDEX 2, LEFT BEST MATCH SHOULD BE PIECE 7 WITH 22 MATCHES, AND RIGHT BEST MATCH SHOULD BE PIECE 6 WITH 14 MATCHES
     FOR PIECE AT INDEX 3, LEFT BEST MATCH SHOULD BE PIECE 1 WITH 29 MATCHES, AND RIGHT BEST MATCH SHOULD BE PIECE 9 WITH 20 MATCHES
     FOR PIECE AT INDEX 4, LEFT BEST MATCH SHOULD BE PIECE 5 WITH 21 MATCHES, AND RIGHT BEST MATCH SHOULD BE PIECE 8 WITH 12 MATCHES
     FOR PIECE AT INDEX 5, LEFT BEST MATCH SHOULD BE PIECE 0 WITH 24 MATCHES, AND RIGHT BEST MATCH SHOULD BE PIECE 4 WITH 21 MATCHES
     FOR PIECE AT INDEX 6, LEFT BEST MATCH SHOULD BE PIECE 2 WITH 14 MATCHES, AND RIGHT BEST MATCH SHOULD BE PIECE 0 WITH 15 MATCHES
     FOR PIECE AT INDEX 7, LEFT BEST MATCH SHOULD BE PIECE 2 WITH 12 MATCHES, AND RIGHT BEST MATCH SHOULD BE PIECE 2 WITH 22 MATCHES
     FOR PIECE AT INDEX 8, LEFT BEST MATCH SHOULD BE PIECE 4 WITH 12 MATCHES, AND RIGHT BEST MATCH SHOULD BE PIECE 1 WITH 14 MATCHES
     FOR PIECE AT INDEX 9, LEFT BEST MATCH SHOULD BE PIECE 3 WITH 20 MATCHES, AND RIGHT BEST MATCH SHOULD BE PIECE 3 WITH 8 MATCHES
     */
    
    
    NSMutableArray *allArrangements = [[NSMutableArray alloc] init];
    
    NSMutableArray *processedArrangements = [[NSMutableArray alloc] init];
    
    for(int i = 0; i < numberOfChunks; i++) {
        
        PieceInfo *piece = piecesInfoArray[i];
        NSLog(@"FOR PIECE AT INDEX %ld, LEFT BEST MATCH SHOULD BE PIECE %ld WITH %ld MATCHES, AND RIGHT BEST MATCH SHOULD BE PIECE %ld WITH %ld MATCHES",
              piece.index,
              piece.leftNeighbourIndex,
              piece.numMatchesWithLeftNeighbour,
              piece.rightNeighbourIndex,
              piece.numMatchesWithRightNeighbour);
  
        [allArrangements addObjectsFromArray:[self reorganizePieceInPosition: piece]];
    }
    //just test the reconstruction for now
    
    NSNumber *number;
    
    int counter = 0;
 
    for(int i =  0, x = 0; i < allArrangements.count && x < numberOfChunks; i++) {
        
        number = [allArrangements objectAtIndex:i];
        if( ![self containsIndexNumber:processedArrangements number:number]) {
            [processedArrangements insertObject:number atIndex:counter];
            counter++;
            reconstructedImagePieces[x] = imagePieces[ number.intValue];
            x++;
        }//else skip this index
    }
    UIImage *reconstructed = [self mergeImagesFromArrayOfImages: reconstructedImagePieces];
    return reconstructed;
}

-(NSMutableArray *) reorganizePieceInPosition: (PieceInfo *) piece {
    
    NSInteger idx = 0;
    NSMutableArray *array  = [[NSMutableArray alloc] init];
    
    if(piece.leftNeighbourIndex > -1) {
        
        NSLog(@"ADING left neighbour %ld at position %ld", piece.leftNeighbourIndex, idx );
        [array insertObject: [NSNumber numberWithInteger: piece.leftNeighbourIndex] atIndex:idx];
        idx++;
    }
    else {
        NSLog(@"INDEX of left is %ld", piece.leftNeighbourIndex);
    }
    
    [array insertObject: [NSNumber numberWithInteger: piece.index] atIndex:idx];
    NSLog(@"ADING %ld at position %ld", piece.index, idx );
    idx++;
    
    
    if(piece.rightNeighbourIndex > -1) {
        [array insertObject: [NSNumber numberWithInteger: piece.rightNeighbourIndex] atIndex:idx];
        NSLog(@"ADING right neighbour %ld at position %ld", piece.rightNeighbourIndex, idx );
    }
    else {
        NSLog(@"INDEX of right is %ld", piece.rightNeighbourIndex);
    }
    
    return array;
}


//merge pieces back in just one final image
- (UIImage *)mergeImagesFromArrayOfImages: (NSArray *)imagesArray {
    
    //get the first as reference for size
    UIImage *exampleImage = [imagesArray firstObject];
    CGSize imageSize = exampleImage.size;
    
    NSLog(@"EXAMPLE IMAGE SIZE, width: %d height: %d", (int)imageSize.width, (int)imageSize.height);
    
    //this is the size of the final image
    CGSize finalSize = CGSizeMake(width,height);
    
    NSLog(@"FINAL SIZE, width: %d height: %d", (int)finalSize.width, (int)finalSize.height);
    //same as the original one, of course
    UIGraphicsBeginImageContext(finalSize);
    
    int xPos = 0;
    //this index multiplied by the chunk width is the X pos of the given chunk on the final image
    
    for (UIImage *image in imagesArray) {
        [image drawInRect: CGRectMake((int)imageSize.width * xPos, 0, imageSize.width, height)];
        xPos+=1;
    }
    
    UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return finalImage;
}


//TODO maybe better attribute a weight
//if all match componennts return the max value
//otherwise return smaller values accordingly (if only the red, or only the blue, for instance)
-(NSUInteger) doRGBValuesMatch: (RGBInfo *) firstColor  otherColor: (RGBInfo *) secondColor  {
    if (firstColor.red == secondColor.red && firstColor.green == secondColor.green && firstColor.blue == secondColor.blue && firstColor.alpha == secondColor.alpha){
        return 4;
    }
    else if(firstColor.red == secondColor.red && firstColor.green == secondColor.green && firstColor.blue == secondColor.blue) {
        return 3;
    }
    else if(firstColor.red == secondColor.red && firstColor.green == secondColor.green){
        return 2;
    }
    else if(firstColor.red == secondColor.red) {
        return 1;
    }
    return 0;
}

//read RGB and alpha from a pixel color
-(RGBInfo *) readRGBValues: (UIColor*) color {
    
     const CGFloat* components = CGColorGetComponents(color.CGColor);
    
    // NSLog(@"Red: %d", (int)components[0]);
    // NSLog(@"Green: %d", (int)components[1]);
    // NSLog(@"Blue: %d", (int)components[2]);
    // NSLog(@"Alpha: %d", (int)CGColorGetAlpha(color.CGColor));
    
    RGBInfo * info = [[RGBInfo alloc] initWithRed:(int)components[0] green:(int)components[1] blue:(int)components[2] andAlpha:(int)CGColorGetAlpha(color.CGColor)];
    return  info;
}



@end
