//
//  main.m
//  TheUnshredder
//
//  Created by PC Dreams on 28/09/2018.
//  Copyright © 2018 PC Dreams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
